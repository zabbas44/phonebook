<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class auth
 * class responsible for handling authentication for users
 * Created : 27th March, 2017
 * Author : Zain
 */

class auth extends CI_Controller {

    /**
     * constructor method
     */

    function __construct()
    {
        parent::__construct();
        $this->load->model('UserModal','',TRUE);
        $this->load->library('form_validation');
    }

    /**
     * function showLogin
     * @param : no param
     * @reponse : loading view for login
     */

    public function index()
    {

        $dataPass['view'] = 'auth/login';
        $this->load->view('layouts/public/master',$dataPass);

    }


    /**
     * function validate
     * @param : username,password
     * response : validate user information from database
     */

    public function validate(){


        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required|callback_validate_database');

        if($this->form_validation->run() == FALSE)
        {
            //Field validation failed.  User redirected to login page
            $dataPass['view'] = 'auth/login';
            $this->load->view('layouts/public/master',$dataPass);
        }
        else
        {
            //redirect to the user internal pages

            redirect('contacts/dashboard', 'refresh');
        }

    }


    /**
     * function callback_validate_database
     * validates information from the database
     * @param $password
     * @param
     * @return bool]
     */

    function validate_database($password)
    {

        //Field validation succeeded.  Validate against database
        $email = $this->input->post('email');

        //query the database
        $result = $this->UserModal->verifyLogin($email, $password);

        if($result)
        {
            /*
             * if user is logged in successfully set the user information into session
             */

            $this->session->set_userdata('logged_in', $result);
            return TRUE;
        }
        else
        {
            /*
             * if user is logged in failed redirect user with error
             */

            $this->form_validation->set_message('validate_database', 'Invalid email or password');
            return false;
        }
    }

    /**
     * function logout
     * @param no param
     * response : logout and redirec user
     */

    public function logout(){

        $this->session->unset_userdata('logged_in');
        redirect('auth','refresh');

    }

}
