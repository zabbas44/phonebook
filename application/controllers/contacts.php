<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Contacts
 * class responsible for contacts CRUD
 * Created : 27th March, 2017
 * Author : Zain
 */

class Contacts extends CI_Controller
{

    /**
     * constructor method
     */
    private $user_id;

    function __construct()
    {
        parent::__construct();

        /**
         * check for the user session, it can be checked using another parent class as i have 1 controller so i will check here
         *
         */

        $this->validateSession();

        $this->load->model('ContactModal', '', TRUE);
        $this->load->library('form_validation');
        $this->load->library('pagination');
    }


    /*
     * function validateSession
     */

    function validateSession(){

        $userData = $this->session->userdata('logged_in');

        if(!isset($userData[0]->id)){
            redirect('auth');
        }else{
            $this->user_id = $userData[0]->id;
        }

    }

    /**
     * function showLogin
     * @param : no param
     * @reponse : loading view for login
     */

    public function dashboard()
    {

        $dataPass['title'] = 'Dashboard';
        $dataPass['desc'] = 'User Dashboard page';
        $dataPass['view'] = 'pages/dashboard';
        $this->load->view('layouts/admin/master', $dataPass);

    }

    /**
     * function Paginatin
     * function Pagination Settings
     * @param
     */

    public function pagination()
    {

        $config = array();
        $total_row = $this->ContactModal->record_count();
        $config["base_url"] = base_url() . "/contacts/view";
        $config["total_rows"] = $total_row;
        $config["per_page"] = 10;

        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = $total_row;
        $config['cur_tag_open'] = '&nbsp;<a class="current">';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';

        $this->pagination->initialize($config);

        if ($this->uri->segment(3)) {

            $start = ($this->uri->segment(3));

            if ($start > 0) {

                $start = $start - 1;

                $start = $config["per_page"] * $start;

            }

        } else {
            $start = 0;
        }

        /*
         * if search filter is applied
         */

        $searchBy   = $this->input->post('filter');
        $searchText = $this->input->post('table_search');

        $data["results"] = $this->ContactModal->get($config["per_page"], $start,false,$searchBy,$searchText,$this->user_id);

        $str_links = $this->pagination->create_links();
        $data["links"] = explode('&nbsp;', $str_links);

        return $data;


    }

    /**
     * function phoneBook
     * @param : no param
     * @reponse : loading listings for contacts
     *
     */

    public function view()
    {


        $dataPass['title'] = 'PhoneBook';
        $dataPass['desc'] = 'User PhoneBook page';
        $dataPass['view'] = 'pages/phonebook/view';
        $dataPass['records'] = $this->pagination();

        $this->load->view('layouts/admin/master', $dataPass);

    }

    /**
     * function add | update case
     * function to load only view for add record
     * @param : no param
     */

    public function add()
    {

        $id   = ($this->uri->segment(3));

        $dataPass['title'] = 'Add Contact';
        $dataPass['desc'] = 'New Contact Addition';

        if(!empty($id)){

            /*
             * if existing data is available
             */

            $response = $this->ContactModal->get(1,0,$id,"","",$this->user_id);

            if(isset($response[0])){
                $dataPass['data'] = $response[0];
                $dataPass['title'] = 'Edit Contact';
                $dataPass['desc'] = 'Edit Contact Addition';
            }

        }


        $dataPass['view'] = 'pages/phonebook/add';
        $this->load->view('layouts/admin/master', $dataPass);

    }

    /**
     * function save
     * function to save new record
     * @param : no param
     */

    public function save()
    {

        $this->form_validation->set_rules('contactname', 'Name', 'required');
        $this->form_validation->set_rules('contactnumber', 'Number', 'required|numeric');
        $this->form_validation->set_rules('contactnotes', 'Notes', 'required');

        $dataPass['title'] = 'Add Contact';
        $dataPass['desc'] = 'New Contact Addition';
        $dataPass['view'] = 'pages/phonebook/add';

        if ($this->form_validation->run() == FALSE) {

            /*
             * error case load the same view
             */

            $this->load->view('layouts/admin/master', $dataPass);

        } else {

            //redirect to the user internal pages

            /**
             * saving record to database
             */

            $data = array(
                'name' => $this->input->post('contactname'),
                'number' => $this->input->post('contactnumber'),
                'notes' => $this->input->post('contactnotes'),
                'added_at' => date('Y-m-d H:i'),
                'updated_at' => date('Y-m-d H:i'),
                'user_id'  => $this->user_id
            );

            /**
             * checking for save and update record as well
             */

            $id   = ($this->uri->segment(3));

            if(!empty($id)){

                /*
                 * if existing data is available
                 */

                $this->session->set_flashdata('success_msg', 'Contact Has Been Successfully Updated');
                $response = $this->ContactModal->update($id,$data);

            }else{

                $this->session->set_flashdata('success_msg', 'Contact Has Been Successfully Added');
                $response = $this->ContactModal->save($data);

            }

            if ($response) {
                redirect('contacts/view', 'refresh');
            } else {

                $this->form_validation->set_message('warning', 'Something Unexpected Happened! Please Try Again');
                $this->load->view('layouts/admin/master', $dataPass);

            }

        }

    }


    /**
     * function remove
     * remove record function
     * @param : no param
     * @return : redirect
     */

    public function remove()
    {

        $id = ($this->uri->segment(3));

        if (!empty($id)) {

            /*
             * delete the record if param is available
             */

            $data = $this->ContactModal->get(1,0,$id,"","",$this->user_id);

            if(is_array($data) && count($data) > 0) {

                $response = $this->ContactModal->remove($id);

                if ($response) {

                    $this->session->set_flashdata('success_msg', 'Record Removed Successfully');
                    redirect('contacts/view', 'refresh');

                } else {

                    $this->session->set_flashdata('success_msg', 'Record Not Removed | Please Retry');
                    redirect('contacts/view', 'refresh');

                }
            }else{

                /*
                * if param is not available
                */

                $this->session->set_flashdata('success_msg', 'Forbidden operation');
                redirect('contacts/view', 'refresh');

            }

        } else {

            /*
             * if param is not available
             */

            $this->session->set_flashdata('success_msg', 'Invalid operation | Please Retry');
            redirect('contacts/view', 'refresh');

        }

    }



}
