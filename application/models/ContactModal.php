<?php

/**
 * Class ContactModal
 * modal class for simple login authentication
 */

Class ContactModal extends CI_Model
{

    private $table = 'contacts';

    function __construct() {
        parent::__construct();
    }

    /**
     * function get
     * function to get records all/single
     * @param $id
     * @param bool $all
     * @param
     * @return bool
     */

    function get($perPage,$start,$id = false,$searchBy = "",$searchText = "",$userId)
    {

        $this->db->limit($perPage, $start);

        if($id <> false){
            $this->db->where('id', $id);
        }


        if(!empty($searchText) && !empty($searchBy)){

            $findBy = '';

            switch($searchBy){

                case 1:
                    $findBy = 'name';
                    break;
                case 2:
                    $findBy = 'number';
                    break;
                case 3:
                    $findBy = 'notes';
                    break;

            }

            $this->db->like($findBy, $searchText);

        }

        $this->db->where('user_id', $userId);

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0) {

            return $query->result();

        } else {
            return array();
        }
    }


    /**
     * function record_count
     * function to get all records
     * @param
     * @return : number of records
     *
     */

    public function record_count(){

        return $this->db->count_all($this->table);

    }

    /**
     * function add
     * function to add record
     * @param $data
     * @param
     */

    function save($data){

        $response = $this->db->insert($this->table, $data);

        if($response){
            return true;
        }

        return false;

    }

    /**
     * function edit
     * function to edit records
     * @param $id
     * @param $data
     * @param
     */

    function update($id,$data){

        $condition = array(
            'id' => $id
        );

        $response = $this->db->update($this->table, $data,$condition);

        return $response;

    }

    /**
     * function remove
     * function to remove record
     * @param $id
     * @param
     */

    function remove($id){

        $condition = array(
            'id' => $id
        );

        $response = $this->db->delete($this->table, $condition);

        return $response;
    }

}