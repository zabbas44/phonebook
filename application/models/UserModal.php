<?php

/**
 * Class UserModal
 * modal class for simple login authentication
 */

Class UserModal extends CI_Model
{

    /**
     * function verifyLogin
     * funtion to check for existing user
     * @param $email
     * @param $password
     * @param
     * @return bool
     */

    function verifyLogin($email, $password)
    {

        $this->db->select('id,first_name,last_name,email');
        $this->db->from('users');
        $this->db->where('email', $email);
        $this->db->where('password', MD5($password));
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

}