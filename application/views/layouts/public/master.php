<?php

/*
 * loading generic header or css files requested
 */

$this->view('layouts/public/header');

/**
 * loading dynamic requested view
 */

$this->view($view);

/**
 * lading generic js/files or other scripts globally
 */

$this->view('layouts/public/footer');


