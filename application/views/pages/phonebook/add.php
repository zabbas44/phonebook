<div class="box-body">

    <?php

    $error   = validation_errors();
    $success =  $this->session->flashdata('success_msg');

    if(!empty($error)){

        ?>
        <div class="alert alert-danger">
            <?php echo $error; ?>
        </div>
    <?php } ?>

    <?php

    if(!empty($success)){

    ?>
    <div class="alert alert-success">
        <?php echo $success; ?>
    </div>
    <?php } ?>

    <?php

    if(isset($data->id)){
        echo form_open('contacts/save/'.$data->id);
    }else{
        echo form_open('contacts/save');
    }

    ?>
    <!-- text input -->
        <div class="form-group">
            <label>Name</label>
            <input class="form-control" <?php if(isset($data->name)) { ?> value="<?php echo $data->name ?>" <?php } ?> name="contactname" placeholder="Please Enter Name" type="text">
        </div>
        <div class="form-group">
            <label>Number</label>
            <input class="form-control" <?php if(isset($data->number)) { ?> value="<?php echo $data->number ?>" <?php } ?> name="contactnumber" placeholder="Please Enter Number" type="number">
        </div>

        <!-- textarea -->
        <div class="form-group">
            <label>Notes</label>
            <textarea class="form-control" name="contactnotes" rows="5" placeholder="Please Enter Notes"><?php if(isset($data->notes)){  ?><?php echo $data->notes ?><?php } ?> </textarea>
        </div>

        <div class="box-footer">

            <?php

                if(isset($data->id)){

                    ?>
                    <input type="submit" class="btn btn-success" value="Update" />

            <?php
                }else{
                    ?>
                    <input type="submit" class="btn btn-success" value="Save" />
            <?php

                }

            ?>

            <input type="reset" class="btn btn-danger" value="Reset" />
        </div>

    <?php
        echo form_close();
    ?>
</div>