<?php

$error = validation_errors();
$success = $this->session->flashdata('success_msg');

if (!empty($error)) {

    ?>
    <div class="alert alert-danger">
        <?php echo $error; ?>
    </div>
<?php } ?>

<?php

if (!empty($success)) {

    ?>
    <div class="alert alert-success">
        <?php echo $success; ?>
    </div>
<?php } ?>

<div class="box-header ">
    <div class="col-md-1 row">
        <a class="btn btn-block btn-primary" href="<?php echo base_url('contacts/add') ?>">Add New</a>
    </div>
    <div class="box-tools">

        <div class="input-group input-group-sm" style="width: 600px;">
            <?php
                echo form_open('contacts/view');
            ?>
            <div class="col-md-4">

                <select class="form-control" name="filter">
                    <option value="1">Search By Name</option>
                    <option value="2">Search By Number</option>
                    <option value="3">Search By Notes</option>
                </select>
            </div>
            <div class="col-md-6">
                <input name="table_search" class="form-control pull-right" placeholder="Search" type="text">

            </div>
            <div class="col-md-2">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>

            <?php
                echo form_close();
            ?>
        </div>
    </div>
</div>
<!-- /.box-header -->
<div class="box-body table-responsive no-padding">
    <table class="table table-hover">
        <tbody>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Number</th>
            <th>Notes</th>
            <th>Date Added</th>
            <th>Actions</th>
        </tr>
        <?php

        if (isset($records['results']) && count($records['results']) > 0) {

            foreach ($records['results'] as $perContact) {
                ?>
                <tr>
                    <td><?php echo $perContact->id; ?></td>
                    <td><?php echo $perContact->name; ?></td>
                    <td><span class="label label-success"><?php echo $perContact->number; ?></span></td>
                    <td><?php echo $perContact->notes; ?></td>
                    <td><?php echo $perContact->added_at; ?></td>
                    <td>
                        <a href="<?php echo base_url('contacts/add/'.$perContact->id); ?>"> Edit </a> | <a href="<?php echo base_url('contacts/remove/'.$perContact->id); ?>"> Remove </a>
                    </td>
                </tr>

            <?php
            }
        } else { ?>

            <div class="alert alert-info alert-dismissible">
                <h4><i class="icon fa fa-info"></i> Alert!</h4>
                No Record Available
            </div>

        <?php } ?>


        </tbody>
    </table>

    <div class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-right">
            <?php foreach ($records['links'] as $link) {
                echo "<li>" . $link . "</li>";
            } ?>
        </ul>
    </div>

</div>
