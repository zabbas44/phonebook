
<div class="login-box">
    <div class="login-logo">
        <b>Login</b>
    </div>
    <!-- /.login-logo -->


    <?php

    $error = validation_errors();

    if(!empty($error)){

    ?>
        <div class="alert alert-danger">
            <?php echo $error; ?>
        </div>
    <?php } ?>


    <div class="login-box-body">

        <p class="login-box-msg">Sign in to start your session</p>

        <?php echo form_open('auth/validate'); ?>
            <div class="form-group has-feedback">
                <input type="email" class="form-control" name="email" placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
               <!-- <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox"> Remember Me
                        </label>
                    </div>
                </div>-->
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
        <?php
            echo form_close();
        ?>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
