<?php

/**
 * Class Migration_Create_Users
 * class to create migration for users table
 */

class Migration_Create_Users extends CI_Migration {

    public function up()
    {
        $fields = array(
            'id int(4) NOT NULL AUTO_INCREMENT',
            'email VARCHAR(80) NOT NULL',
            'password VARCHAR(120) NOT NULL',
            'enabled bool NOT NULL DEFAULT 1',
            'created_at date_time DEFAULT CURRENT_TIMESTAMP',
            'updated_at date_time DEFAULT CURRENT_TIMESTAMP'
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->create_table('users');
    }

    public function down()
    {
        $this->dbforge->drop_table('users');
    }

}