
DOCUMENTATION TO INSTALL APPLICATION ON YOUR SYSTEM

SYSTEM REQUIREMENTS :

        1 - It should work on 5.3.7 as well, but we strongly advise you NOT to run such old versions of PHP, because of potential security and performance issues, as well as missing features.
        2 - Mysql Database

SYSTEM INSTALLATION GUIDE :

STEP 1 :
        CLONE THE CODE TO YOUR DESTINATION PATH, MAKE SURE YOU HAVE SUCCESSFULLY COMPLETED THE CODE CLONE.

STEP 2 :
        DATABASE FILE IS AVAILABLE UNDER database folder, open PHPMYADMIN/OR ANY OTHER MYSQL EDITOR and import the database file.

STEP 3 :
        OPEN THE FOLLOWING FILES TO CHANGE TO YOUR LOCAL SYSTEM SETTINGS ENVIRONMENT

            1 - application/config/config.php (change your required base_url to the machine localhost )
            2 - application/config/database.php

                    1 -      hostname' => 'to your localhost server most of time its "localhost"',
                    2 -    	'username' => 'to your database created username',
                    3 -    	'password' => 'to your database password',
                    5 -    	'database' => 'change this to database name',

STEP 4 :
        AFTER SUCCESSFULLY COMPLETING ABOVE POINTS PLEASE OPEN YOUR BROWSER AND TYPE IN PROVIDED LOCALHOST NAME IN YOUR BROWSER
        FOR EXAMPLE : http://phone.local/ OR LOCALHOST/APPLICATIONNAME

STEP 5 :
        IF YOU ARE USING SUB FOLDER ON LOCALHOST, PLEASE MAKE SURE TO ADJUST YOUT .htaccess file accordingly to run project


LOGIN CREDENTIALS INFORMATION :

        FOR TESTING PURPOSES I HAVE CREATED 2 USER WHICH YOU CAN USE TO LOGIN INTO APPLICATION

        1 -
            Username : test@email.com
            Password : admin

        2 - Username : loka@loka.com
            Password : admin

AFTER LOGIN PROCESS :

        ONCE YOU ARE LOGGED INTO APPLICATION, CLICK ON LEFT SIDE PHONEBOOK MENU TO EXPAND THE OPTIONS

            1 - ADD    (TO ADD NEW RECORD)
            2 - VIEWALL (TO VIEW ALL RECORDS)


STILL FOUND SOME PROBLEM :

        PLEASE CONTACT OVER SKYPE (zainzain185) | EMAIL (zabbas44@gmail.com)

Zain